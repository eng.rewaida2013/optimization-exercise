defmodule CommonLib do

  @elixir_org "https://elixir-lang.org/"

  # gets Elixir introduction from https://elixir-lang.org/ dynamically
  def get_elixir_stuff() do

    case HTTPoison.get(@elixir_org) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        elixir_stuff =
        body
        |> Floki.find("div.entry-summary")
        |> Floki.find("h5")
        |> Floki.text()
        {:ok, elixir_stuff}
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        {:error, "Not found :("}
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, IO.inspect reason}
    end

  end

end
