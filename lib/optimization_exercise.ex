defmodule OptimizationExercise do
  @founder_one_name "Fleur"
  @founder_two_name "Charlotte"
  @factor 3
  @config %{
    general: %{
      this_is_ok: false
    },
    other_important_namespace: %{}
  }

  # returns greeting using first/last name
  @spec greet(String.t, String.t) :: String.t
  def greet(first_name, last_name) do

    if(first_name == nil) do
      "Hello, mr/ms #{last_name},"
    else
      "Hello, #{first_name} #{last_name},"
    end

  end

  # returns introduction, you can pass one or two paramters maximum.
  @spec introduce(String.t, String.t) :: String.t
  def introduce(first_list_item, second_list_item \\ "") do
    header = "In this exercise, you'll be showing what you can do\n" <>
      "towards optimizing an existing Elixir codebase. Also, you will be:\n" <>
    "- " <> first_list_item <> "\n"

    footer = "Did you notice that I am quite familiar with function arity?"

    if second_list_item != "" do
      header <> "- " <> second_list_item <> "\n" <> footer
    else
      header <> footer
    end

  end

  # this is another solution where you can pass list of items
  # and returns the items formatted in the same way as in introduct/2
  @spec introduce_list(list) :: String.t
  def introduce_list(intro_list) do
    "In this exercise, you'll be showing what you can do\n" <>
      "towards optimizing an existing Elixir codebase. Also, you will be:\n" <>

    "- " <>  Enum.join(intro_list, "\n- ") <>

    "\nDid you notice that I am quite familiar with function arity?"
  end

  # educates about Equaluture and Elixir
  @spec educate :: String.t
  def educate do
    equalture_stuff = "Equalture was founded in 2017 by twin sisters " <>
    "#{@founder_one_name} and #{@founder_two_name}.\n"<>
    "By 2019 they had ditched most of their PHP codebase in favor of Elixir."

    elixir_stuff = elem(CommonLib.get_elixir_stuff(), 1)

    Enum.join([equalture_stuff, elixir_stuff], "\n")
  end

  # returns list of number multiplied by factor 3
  @spec multiply_list(list) :: list
  def multiply_list(list) do
    Enum.map(list, &(&1 * @factor))
  end

  # returns subvalue from config attribute
  @spec get_config_subvalue(any, any) :: any
  def get_config_subvalue(namespace, key) do
    values = Map.fetch(@config, namespace)

    if values === :error do
      nil
    else
      value = Map.fetch(elem(values,1), key)
      if value === :error do
        nil
      else
        elem(value,1)
      end
    end

  end

end
