defmodule OptimizationExerciseTest do
  use ExUnit.Case
  doctest OptimizationExercise

  describe "greet/2" do
    test "uses the full name to greet you" do
      assert OptimizationExercise.greet("Jose", "Valim") ==
        "Hello, Jose Valim,"
    end

    test "is polite when only last name is given" do
      assert OptimizationExercise.greet(nil, "Valim") ==
        "Hello, mr/ms Valim,"
    end
  end

  describe "introduce_list/1" do
    test "introduces the project" do
      assert OptimizationExercise.introduce_list(["learning something about Equalture",
       "improving this specific function, because it needs it"]) ==
        "In this exercise, you'll be showing what you can do
towards optimizing an existing Elixir codebase. Also, you will be:
- learning something about Equalture
- improving this specific function, because it needs it
Did you notice that I am quite familiar with function arity?"
    end
  end

  describe "introduce/1" do
    test "introduces the project" do
      assert OptimizationExercise.introduce("learning something about Equalture") ==
        "In this exercise, you'll be showing what you can do
towards optimizing an existing Elixir codebase. Also, you will be:
- learning something about Equalture
Did you notice that I am quite familiar with function arity?"
    end
  end

  describe "introduce/2" do
    test "introduces the project" do
      assert OptimizationExercise.introduce(
        "learning something about Equalture",
        "improving this specific function, because it needs it") ==
        "In this exercise, you'll be showing what you can do
towards optimizing an existing Elixir codebase. Also, you will be:
- learning something about Equalture
- improving this specific function, because it needs it
Did you notice that I am quite familiar with function arity?"
    end
  end

  describe "educate/0" do
    test "educates about Equalture and Elixir" do
      result = CommonLib.get_elixir_stuff()

      assert OptimizationExercise.educate() ==
      "Equalture was founded in 2017 by twin sisters Fleur and Charlotte.
By 2019 they had ditched most of their PHP codebase in favor of Elixir.
#{elem(result, 1)}" && elem(result,0) == :ok
    end
  end

  describe "multiply_list/1" do
    test "multiplies every item in a list by 3" do
      assert OptimizationExercise.multiply_list([1, 2, 3]) ==
        [3, 6, 9]
    end
  end

  describe "get_config_subvalue/2" do
    test "gets subvalue from config attribute" do
      refute OptimizationExercise.get_config_subvalue(:general, :this_is_ok)
    end

    test "returns nil when value is not found" do
      assert OptimizationExercise.get_config_subvalue(:general, :this_is_not_ok) == nil
      assert OptimizationExercise.get_config_subvalue(:other_important_namespace, :foo) == nil
    end
  end
end
